package ru.mirsaitov.requestor;

public class Main {

    public static void main(String[] args) {
        Requestable requestable = new Requestor();
        requestable.getJokes(requestable.getCategories()).stream()
                .forEach(joke -> System.out.println("Category: " + joke.getCategory() + " joke: " + joke.getJoke()));

    }

}
