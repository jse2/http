package ru.mirsaitov.requestor;

public interface ParseService {

    String[] parseCategories(String body);

    Joke parseJoke(String body);

}
