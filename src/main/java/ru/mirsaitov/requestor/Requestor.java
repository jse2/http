package ru.mirsaitov.requestor;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.HttpClient.Redirect;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse.BodyHandlers;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class Requestor implements Requestable {

    private static String URL = "https://api.chucknorris.io/jokes/";

    private static String CATEGORIES = "categories";

    private static String RANDOM_CATEGORY_JOKES = "random?category=#";

    private final ParseService parseService = new ParseServiceImpl();

    private  HttpClient client = HttpClient.newBuilder()
            .version(Version.HTTP_1_1)
            .followRedirects(Redirect.NORMAL)
            .connectTimeout(Duration.ofSeconds(20))
            .build();

    @Override
    public String[] getCategories() {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(URL.concat(CATEGORIES)))
                .timeout(Duration.ofMinutes(2))
                .header("Content-Type", "application/json")
                .GET()
                .build();
        try {
            HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
            return parseService.parseCategories(response.body());
        } catch (IOException | InterruptedException exception) {
            System.out.println("Error request");
        }
        return new String[0];
    }

    @Override
    public List<Joke> getJokes(String[] categories) {
        List<Joke> jokes = new LinkedList<>();

        if (categories == null || categories.length == 0) {
            return jokes;
        }
        List<CompletableFuture<Joke>> list = Arrays.stream(categories)
                .map(category -> URI.create(URL.concat(RANDOM_CATEGORY_JOKES.replace("#", category))))
                .map(uri -> HttpRequest.newBuilder()
                        .uri(uri)
                        .timeout(Duration.ofMinutes(2))
                        .header("Content-Type", "application/json")
                        .GET()
                        .build())
                .map(httpRequest -> client.sendAsync(httpRequest, BodyHandlers.ofString())
                        .thenApply(HttpResponse::body)
                        .thenApply(parseService::parseJoke))
                .collect(Collectors.toList());

        try {
            CompletableFuture.allOf(list.toArray(new CompletableFuture[0])).get();
        } catch (InterruptedException | ExecutionException exception) {
            return jokes;
        }

        for (CompletableFuture<Joke> joke : list) {
            try {
                jokes.add(joke.get());
            } catch (InterruptedException | ExecutionException exception) {
            }
        }
        return jokes;
    }

}
