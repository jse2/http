package ru.mirsaitov.requestor;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class ParseServiceImpl implements ParseService {

    private final Gson gson = new Gson();

    @Override
    public String[] parseCategories(String body) {
        if (body == null || body.isEmpty()) {
            return new String[0];
        }
        return gson.fromJson(body, String[].class);
    }

    @Override
    public Joke parseJoke(String body) {
        JsonObject object = gson.fromJson(body, JsonObject.class);
        return new Joke(object.get("categories").getAsJsonArray().get(0).getAsString(), object.get("value").getAsString());
    }

}
