package ru.mirsaitov.requestor;

import java.util.List;

public interface Requestable {

    String[] getCategories();

    List<Joke> getJokes(String[] categories);

}