package ru.mirsaitov.requestor;

public class Joke {

    private String category;

    private String joke;

    public Joke(String category, String joke) {
        this.category = category;
        this.joke = joke;
    }

    public String getCategory() {
        return category;
    }

    public String getJoke() {
        return joke;
    }

}
